package com.krs.aidlexample.receiver;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.os.RemoteException;
import android.util.Log;

import com.krs.aidlexample.IRemote;
import com.krs.aidlexample.MainObject;

import java.util.ArrayList;
import java.util.List;


public class RemoteService extends Service {

    private final IRemote.Stub mBinder = new IRemote.Stub() {

        @Override
        public int add(int a, int b) throws RemoteException {
            log("Received add command.");
            return (a + b);
        }

        @Override
        public void exit() throws RemoteException {
            log("Received exit command.");
            stopSelf();
        }

        @Override
        public MainObject[] listFiles(String path) throws RemoteException {
            log("Received list command for: " + path);

            List<MainObject> toSend = new ArrayList<>();
            for (int i = 0; i < 100; i++)
                toSend.add(new MainObject(path + (i + 1)));
            return toSend.toArray(new MainObject[toSend.size()]);
        }
    };

    private void log(String message) {
        Log.v("RemoteService", message);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        log("Received start command.");
        return START_STICKY;
    }

    @Override
    public IBinder onBind(Intent intent) {
        log("Received binding.");
        return mBinder;
    }
}