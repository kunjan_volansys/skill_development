package com.krs.aidlexample;

import com.krs.aidlexample.MainObject;
interface IRemote
{
	  int add(int a, int b);

	  void exit();

	  MainObject[] listFiles(String path);
}