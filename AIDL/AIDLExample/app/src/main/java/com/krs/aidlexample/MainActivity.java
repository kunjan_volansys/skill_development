package com.krs.aidlexample;

import android.app.Activity;
import android.app.Service;
import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.os.RemoteException;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;


public class MainActivity extends Activity implements View.OnClickListener {

    protected IRemote mService;
    EditText mFirst, mSecond;
    Button mAdd;
    TextView mResultText;
    ServiceConnection mServiceConnection;
    String TAG = MainActivity.class.getSimpleName();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mFirst = findViewById(R.id.firstValue);
        mSecond = findViewById(R.id.secondValue);
        mResultText = findViewById(R.id.resultText);
        mAdd = findViewById(R.id.add);
        mAdd.setOnClickListener(this);

        initConnection();

    }

    void initConnection() {
        mServiceConnection = new ServiceConnection() {

            @Override
            public void onServiceDisconnected(ComponentName name) {
                mService = null;
                Toast.makeText(getApplicationContext(), "no", Toast.LENGTH_SHORT).show();
                Log.d(TAG, "Binding - Service disconnected");
            }

            @Override
            public void onServiceConnected(ComponentName name, IBinder service) {
                mService = IRemote.Stub.asInterface((IBinder) service);
                Toast.makeText(getApplicationContext(), "yes", Toast.LENGTH_SHORT).show();
                Log.d(TAG, "Binding is done - Service connected");
                performListing();
            }
        };
        if (mService == null) {
            Intent it = new Intent();
            it.setComponent(new ComponentName("com.krs.aidlexample.receiver",
                    "com.krs.aidlexample.receiver.RemoteService"));
            //it.setAction("com.krs.service.CALCULATOR");
            startService(it);
            bindService(it, mServiceConnection, Service.BIND_AUTO_CREATE);
        }
    }

    private void performListing() {

        try {
            MainObject[] results = mService.listFiles("/sdcard/testing");
            Log.v(TAG, "Received " + results.length + " results");
        } catch (RemoteException e) {
            e.printStackTrace();
        }

        try {
            mService.exit();
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }


    protected void onDestroy() {

        super.onDestroy();
        unbindService(mServiceConnection);
    }

    ;

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.add: {
                int a = Integer.parseInt(mFirst.getText().toString());
                int b = Integer.parseInt(mSecond.getText().toString());

                try {
                    mResultText.setText("Result -> Add ->" + mService.add(a, b));
                    Log.d(TAG, "Binding - Add operation");
                } catch (RemoteException e) {
                    e.printStackTrace();
                }
            }
            break;
        }
    }
}
