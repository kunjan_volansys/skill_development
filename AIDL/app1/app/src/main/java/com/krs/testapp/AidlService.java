package com.krs.testapp;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.os.RemoteException;

public class AidlService extends Service {
    public AidlService() {
    }

    @Override
    public IBinder onBind(Intent intent) {
        return new MultiplyNumberAidl.Stub() {
            @Override
            public int multiply(int firstNumber, int secondNumber) throws RemoteException {
                return (firstNumber * secondNumber);
            }

            @Override
            public void basicTypes(int anInt, long aLong, boolean aBoolean, float aFloat, double aDouble, String aString) throws RemoteException {

            }
        };
    }
}
