// MultiplyNumberAidl.aidl
package com.krs.testapp;

// Declare any non-default types here with import statements

interface MultiplyNumberAidl {

     int multiply(in int firstNumber, in int secondNumber);

    /**
     * Demonstrates some basic types that you can use as parameters
     * and return values in AIDL.
     */
    void basicTypes(int anInt, long aLong, boolean aBoolean, float aFloat,
            double aDouble, String aString);
}
