package com.krs.messanger;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

/**
 * Created by kunjan on 29/1/18.
 */

public class BindServiceIPCActivity extends AppCompatActivity {

    private Button btnSend;
    private EditText editText;
    private Messenger messenger;
    private boolean isBound;
    private final Messenger mActivityMessenger = new Messenger(new ResponseTakeHandler(this));


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bind_ipc);

        editText = findViewById(R.id.messageEditText);
        btnSend =  findViewById(R.id.sendButton);
        btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String messageText = editText.getText().toString();
                if (messageText.isEmpty()){
                    Toast.makeText(BindServiceIPCActivity.this, "Please enter message", Toast.LENGTH_LONG).show();
                }else{
                    Message message = Message.obtain(null,MyServiceIPC.JOB_1);
                    Bundle bundle = new Bundle();
                    bundle.putString("message", messageText);
                    message.setData(bundle);
                    // store messenger object to message property "replyTo"
                    message.replyTo = mActivityMessenger;
                    try {
                        messenger.send(message);
                    } catch (RemoteException e) {
                        e.printStackTrace();
                    }
                }

            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (!isBound) {
            // start service here
            Intent intent = new Intent(BindServiceIPCActivity.this, MyServiceIPC.class);
            bindService(intent, serviceConnection, Context.BIND_AUTO_CREATE);
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        isBound = false;
        messenger = null;
    }

    ServiceConnection serviceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            messenger = new Messenger(service);
            isBound = true;
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            isBound = false;
            messenger = null;
        }
    };


    public class ResponseTakeHandler extends Handler {

        public ResponseTakeHandler(Context context){

        }
        @Override
        public void handleMessage(Message msg) {

            switch (msg.what){
                case MyServiceIPC.JOB_RESPONSE_1:
                    String result = msg.getData().getString("message_res");
                    Toast.makeText(BindServiceIPCActivity.this, "Response: "+result, Toast.LENGTH_LONG).show();
                    break;
                default:
                    super.handleMessage(msg);
            }
        }
    }
}
